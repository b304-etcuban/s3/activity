package com.zuitt.WDC043_S3_A1;

import java.util.Scanner;

public class FactorialNumber {
    public static void main(String[] args) {
        System.out.println("Enter an integer to compute its factorial:");
        Scanner in = new Scanner(System.in);
        int num = 0;
        try {
            num = in.nextInt();
            if (num < 0) {
                System.out.println("Invalid input. Please enter a non-negative integer.");
                return;
            } else if (num == 0) {
                System.out.println("Factorial of 0 is 1.");
                return;
            }
            int answer = 1;
            int counter = 1;
            while (counter <= num) {
                answer *= counter;
                counter++;
            }

            System.out.println("Factorial of " + num + " using while loop: " + answer);

            // solution using for loop
            answer = 1;
            for (int i = 1; i <= num; i++) {
                answer *= i;
            }

            System.out.println("Factorial of " + num + " using for loop: " + answer);
        } catch (Exception e) {
            System.out.println("Invalid input. Please enter an integer.");
        }
    }
}
